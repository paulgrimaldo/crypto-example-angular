import {Component} from '@angular/core';
import CryptoJS from 'crypto-js';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'crypto-examples-angular';
  fileToHash: any;
  fileReader = new FileReader();
  fileTitle = 'File to use';
  calculatedHash: string;

  constructor() {
    this.fileReader.onloadend = (e) => this.onHashCalculated(e);
  }

  onHashCalculated(e): void {
    if (e.target.readyState !== FileReader.DONE) {
      return;
    }
    const wordArray = CryptoJS.lib.WordArray.create(this.fileReader.result);
    this.calculatedHash = CryptoJS.SHA256(wordArray).toString(CryptoJS.enc.Hex);
  }

  hashFile(): void {
    this.fileReader.readAsArrayBuffer(this.fileToHash);
  }

  onSelectFile($event): void {
    this.calculatedHash = null;
    if ($event == null) {
      this.fileTitle = 'File to use';
      return;
    }
    this.calculatedHash = null;
    this.fileToHash = $event.target.files[0];
    this.fileTitle = this.fileToHash.name;
  }
}
